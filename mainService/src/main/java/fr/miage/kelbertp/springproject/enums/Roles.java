package fr.miage.kelbertp.springproject.enums;

public class Roles {

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";

    private Roles() {
    }
}
