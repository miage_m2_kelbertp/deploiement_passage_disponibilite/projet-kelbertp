package fr.miage.kelbertp.bankservice.enums;

public class ErrorMessages {
    private ErrorMessages() {
    }
    public static final String BANK_ACCOUNT_NOT_FOUND = "{\"message\": \"Bank account not found\"}";
    public static final String NO_ENOUGH_MONEY = "{\"message\": \"No enough money\"}";
}
