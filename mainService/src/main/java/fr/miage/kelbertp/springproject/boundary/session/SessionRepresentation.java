package fr.miage.kelbertp.springproject.boundary.session;

import fr.miage.kelbertp.springproject.boundary.conference.ConferenceResource;
import fr.miage.kelbertp.springproject.control.JwtCheck;
import fr.miage.kelbertp.springproject.control.assembler.SessionAssembler;
import fr.miage.kelbertp.springproject.entities.conference.Conference;
import fr.miage.kelbertp.springproject.entities.session.Session;
import fr.miage.kelbertp.springproject.entities.session.validator.SessionInput;
import fr.miage.kelbertp.springproject.entities.session.validator.SessionValidator;
import fr.miage.kelbertp.springproject.enums.Roles;
import jakarta.transaction.Transactional;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static fr.miage.kelbertp.springproject.enums.ErrorMessages.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@ResponseBody
@RequestMapping(value = "/conferences/{idConference}/sessions", produces = "application/json")
public class SessionRepresentation {

    private final SessionAssembler sa;
    private final SessionResource sr;
    private final SessionValidator sv;
    private final ConferenceResource cr;

    public SessionRepresentation(SessionAssembler sa, SessionResource sr, SessionValidator sv, ConferenceResource cr) {
        this.sa = sa;
        this.sr = sr;
        this.sv = sv;
        this.cr = cr;
    }

    @PostMapping
    @Transactional
    public ResponseEntity<Object> createSession(@RequestHeader("Authorization") String auth, @RequestBody @Valid SessionInput session, @PathVariable String idConference) {
        if (!JwtCheck.userCanAccess(auth, List.of(Roles.ROLE_ADMIN))) return ResponseEntity.status(401).build();

        Optional<Conference> conference = cr.findById(idConference);
        if (conference.isEmpty()) return ResponseEntity.status(404).body("Conference not found");

        Session sessionToSave = sa.toEntity(session, conference.get());
        Session s = sr.save(sessionToSave);

        HashMap<String, String> pathVariables = new HashMap<>();
        pathVariables.put("idConference", idConference);

        URI uri = linkTo(SessionRepresentation.class, pathVariables).slash(s.getId()).toUri();

        return ResponseEntity.created(uri).body(sa.toModel(s));
    }

    @GetMapping
    public ResponseEntity<Object> getAllSessions(@PathVariable String idConference) {
        Optional<Conference> conference = cr.findById(idConference);
        return conference
                .<ResponseEntity<Object>>map(
                        value -> ResponseEntity.ok(sa.toCollectionModel(sr.findAllByConference(value)))
                )
                .orElseGet(
                        () -> ResponseEntity.status(404).body(CONF_NOT_FOUND)
                );
    }

    @GetMapping("/{idSession}")
    public ResponseEntity<Object> getSession(@PathVariable String idConference, @PathVariable String idSession) {
        Optional<Conference> conference = cr.findById(idConference);
        if (conference.isEmpty()) return ResponseEntity.status(404).body(CONF_NOT_FOUND);

        Optional<Session> session = sr.findById(idSession);
        if (session.isEmpty()) return ResponseEntity.status(404).body(SESSION_NOT_FOUND);

        if (!session.get().getConference().getId().equals(idConference))
            return ResponseEntity.status(403).body(CANT_ACCESS_TO_RESOURCE);
        return ResponseEntity.ok(sa.toModel(session.get()));
    }

    @PatchMapping("/{idSession}")
    @Transactional
    public ResponseEntity<Object> patchSession(@RequestHeader("Authorization") String auth, @RequestBody SessionInput session, @PathVariable String idConference, @PathVariable String idSession) {
        if (!JwtCheck.userCanAccess(auth, List.of(Roles.ROLE_ADMIN))) return ResponseEntity.status(401).build();

        Optional<Conference> conference = cr.findById(idConference);
        if (conference.isEmpty()) return ResponseEntity.status(404).body(CONF_NOT_FOUND);

        Optional<Session> sessionToUpdate = sr.findById(idSession);
        if (sessionToUpdate.isEmpty()) return ResponseEntity.status(404).body(SESSION_NOT_FOUND);

        if (!sessionToUpdate.get().getConference().getId().equals(idConference))
            return ResponseEntity.status(403).body(CANT_ACCESS_TO_RESOURCE);

        try {
            sv.validateProperties(session);
        } catch (ConstraintViolationException e) {
            return ResponseEntity.status(400).body(e.getMessage());
        }

        sessionToUpdate.map(s -> {
            if (session.getCapacity() != 0) s.setCapacity(session.getCapacity());
            if (session.getDate() != null) s.setDate(session.getDate());
            if (session.getPrice() != 0) s.setPrice(session.getPrice());
            return sr.save(s);
        });

        return ResponseEntity.ok(sa.toModel(sessionToUpdate.get()));
    }
}
