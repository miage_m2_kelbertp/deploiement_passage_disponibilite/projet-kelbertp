package fr.miage.kelbertp.springproject.boundary.session;

import fr.miage.kelbertp.springproject.entities.conference.Conference;
import fr.miage.kelbertp.springproject.entities.session.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SessionResource extends JpaRepository<Session, String> {

    @Query("SELECT s FROM Session s WHERE s.conference = :conference")
    Iterable<Session> findAllByConference(Conference conference);
}
