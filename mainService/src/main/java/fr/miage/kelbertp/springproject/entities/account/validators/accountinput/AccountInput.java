package fr.miage.kelbertp.springproject.entities.account.validators.accountinput;


import fr.miage.kelbertp.springproject.exceptions.ValidationsMessages;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static fr.miage.kelbertp.springproject.exceptions.ValidationsMessages.PROPERTY_CANNOT_BE_BLANK;
import static fr.miage.kelbertp.springproject.exceptions.ValidationsMessages.PROPERTY_IS_REQUIRED;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountInput {
    @NotNull(message = PROPERTY_IS_REQUIRED)
    @NotBlank(message = PROPERTY_CANNOT_BE_BLANK)
    private String lastName;
    @NotNull(message = PROPERTY_IS_REQUIRED)
    @NotBlank(message = PROPERTY_CANNOT_BE_BLANK)
    private String firstName;
    @NotNull(message = PROPERTY_IS_REQUIRED)
    @NotBlank(message = PROPERTY_CANNOT_BE_BLANK)
    @Email(message = ValidationsMessages.EMAIL_MUST_BE_VALID)
    private String email;
    @Size(min = 8, message = ValidationsMessages.PASSWORD_MUST_BE_AT_LEAST_8_CHARACTERS_LONG)
    @NotBlank(message = PROPERTY_CANNOT_BE_BLANK)
    @NotNull(message = PROPERTY_IS_REQUIRED)
    private String password;
}
