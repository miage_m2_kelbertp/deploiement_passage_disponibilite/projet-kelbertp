package fr.miage.kelbertp.springproject.control.assembler;

import fr.miage.kelbertp.springproject.boundary.register.RegisterRepresentation;
import fr.miage.kelbertp.springproject.boundary.user.AccountRepresentation;
import fr.miage.kelbertp.springproject.entities.account.Account;
import fr.miage.kelbertp.springproject.entities.register.Register;
import fr.miage.kelbertp.springproject.entities.session.Session;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class RegisterAssembler implements RepresentationModelAssembler<Register, EntityModel<Register>> {

    public Register toEntity(Account account, Session session) {
        return new Register(
                UUID.randomUUID().toString(),
                account,
                session,
                null
        );
    }

    @Override
    @NonNull
    public EntityModel<Register> toModel(@NonNull Register register) {
        return EntityModel.of(register,
                linkTo(methodOn(RegisterRepresentation.class).patchRegister(
                        null,
                        register.getSession().getConference().getId(),
                        register.getSession().getId(),
                        register.getId(),
                        null
                )).withRel("payment"),
                linkTo(methodOn(AccountRepresentation.class).getAllRegisters(
                        null,
                        register.getAccount().getId()
                )).withRel("allRegisters")
        );
    }
}
