package fr.miage.kelbertp.bankservice.boundary;

import fr.miage.kelbertp.bankservice.entities.bankaccount.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BankResource extends JpaRepository<BankAccount, String> {

    @Query("SELECT ba FROM BankAccount ba WHERE ba.idAccount = :idAccount")
    BankAccount findByIdAccount(String idAccount);
}
