package fr.miage.kelbertp.springproject.boundary.user;

import fr.miage.kelbertp.springproject.boundary.register.RegisterResource;
import fr.miage.kelbertp.springproject.control.ExternalCall;
import fr.miage.kelbertp.springproject.control.JwtCheck;
import fr.miage.kelbertp.springproject.control.assembler.AccountAssembler;
import fr.miage.kelbertp.springproject.control.assembler.RegisterAssembler;
import fr.miage.kelbertp.springproject.entities.account.Account;
import fr.miage.kelbertp.springproject.entities.account.validators.accountinput.AccountInput;
import fr.miage.kelbertp.springproject.entities.account.validators.accountinput.AccountValidator;
import fr.miage.kelbertp.springproject.entities.account.validators.postlogin.AccountLoginInput;
import fr.miage.kelbertp.springproject.entities.account.validators.postlogin.AccountValidatorPostLogin;
import fr.miage.kelbertp.springproject.entities.register.Register;
import fr.miage.kelbertp.springproject.enums.Roles;
import jakarta.transaction.Transactional;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.*;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@ResponseBody
@RequestMapping(value = "/accounts", produces = MediaType.APPLICATION_JSON_VALUE)
public class AccountRepresentation {

    private final AccountResource ar;
    private final AccountAssembler aa;
    private final AccountValidator av;
    private final AccountValidatorPostLogin alv;
    private final RegisterResource rr;
    private final RegisterAssembler ra;
    private final RestTemplate restTemplate;
    private static final String BANKS = "/banks/";

    public AccountRepresentation(AccountResource ar, AccountAssembler aa, AccountValidator av, AccountValidatorPostLogin alv, RegisterResource rr, RegisterAssembler ra, RestTemplate restTemplate) {
        this.ar = ar;
        this.aa = aa;
        this.av = av;
        this.alv = alv;
        this.rr = rr;
        this.ra = ra;
        this.restTemplate = restTemplate;
    }

    @GetMapping(value = "/{accountId}")
    public ResponseEntity<EntityModel<Account>> getAccount(@PathVariable String accountId) {
        return ar.findById(accountId)
                .map(aa::toModelWithoutToken)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<CollectionModel<EntityModel<Account>>> getAllAccounts() {
        return ResponseEntity.ok(aa.toCollectionModel(ar.findAll()));
    }

    @PostMapping
    @Transactional
    public HttpEntity<?> createUser(@RequestBody @Valid AccountInput account, @Value("${bank.url}") String bankUrl) {
        av.validate(account);
        Account accountToSave;
        accountToSave = aa.toEntity(account);

        String url = bankUrl + BANKS + accountToSave.getId() + "/create";
        ResponseEntity<String> resCreateBankAccount = ExternalCall.call(restTemplate, url, HttpMethod.POST, null);

        if (!resCreateBankAccount.getStatusCode().is2xxSuccessful()) {
            return resCreateBankAccount;
        }

        Account u = ar.save(accountToSave);

        URI uri = linkTo(AccountRepresentation.class).slash(u.getId()).toUri();
        return ResponseEntity.created(uri).body(aa.toModel(u));
    }

    @PatchMapping(value = "/{accountId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public ResponseEntity<Object> updateUser(@PathVariable String accountId, @RequestBody AccountInput account) {
        // Validate each property if it is present
        try {
            av.validateProperties(account);
        } catch (ConstraintViolationException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        Optional<Account> accountToUpdate = ar.findById(accountId);
        if (accountToUpdate.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        accountToUpdate.map(u -> {
            if (StringUtils.hasText(account.getLastName())) {
                u.setLastName(account.getLastName());
            }

            if (StringUtils.hasText(account.getFirstName())) {
                u.setFirstName(account.getFirstName());
            }

            if (StringUtils.hasText(account.getEmail())) {
                u.setEmail(account.getEmail());
            }

            if (StringUtils.hasText(account.getPassword())) {
                u.setPassword(account.getPassword());
            }

            return u;
        });

        return ResponseEntity.ok(aa.toModel(accountToUpdate.get()));
    }

    @PostMapping(value = "/login")
    @Transactional
    public ResponseEntity<Object> login(@RequestBody @Valid AccountLoginInput accountLogin) {
        alv.validate(accountLogin);

        Account account = ar.findAccountByEmailAndPassword(accountLogin.getEmail(), accountLogin.getPassword());
        if (account == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("{ \"message\": \"Wrong email or password\" }");
        }

        return ResponseEntity.ok(aa.toModel(account));
    }

    @GetMapping(value = "/{accountId}/registers")
    public ResponseEntity<CollectionModel<EntityModel<Register>>> getAllRegisters(
            @RequestHeader("Authorization") String auth,
            @PathVariable String accountId) {
        if (!JwtCheck.userCanAccess(auth, List.of(Roles.ROLE_ADMIN, Roles.ROLE_USER))) return ResponseEntity.status(401).build();

        Optional<Account> account = ar.findById(accountId);
        if (account.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(ra.toCollectionModel(rr.getRegisterByAccount(account.get())));
    }

    @PatchMapping(value = "/{accountId}/add-money")
    @Transactional
    public ResponseEntity<String> patchUserBalance(
            @RequestHeader("Authorization") String auth,
            @RequestParam(value = "amount") Double amount,
            @PathVariable("accountId") String accountId,
            @Value("${bank.url}") String bankUrl
    ) {
        if (!JwtCheck.userCanAccess(auth, List.of(Roles.ROLE_ADMIN, Roles.ROLE_USER)))
            return ResponseEntity.status(401).build();

        Optional<Account> account = ar.findById(accountId);
        if (account.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        String json = "{ \"amount\": " + amount + " }";
        String url = bankUrl + BANKS + accountId;

        return ExternalCall.call(restTemplate, url, HttpMethod.POST, json);
    }

    @GetMapping(value = "/{accountId}/balance")
    public ResponseEntity<String> getAccountBalance(
            @RequestHeader("Authorization") String auth,
            @PathVariable("accountId") String accountId,
            @Value("${bank.url}") String bankUrl
    ) {
        if (!JwtCheck.userCanAccess(auth, List.of(Roles.ROLE_ADMIN, Roles.ROLE_USER)))
            return ResponseEntity.status(401).build();

        String url = bankUrl + BANKS + accountId;

        return ExternalCall.call(restTemplate, url, HttpMethod.GET, null);
    }
}
