package fr.miage.kelbertp.springproject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.miage.kelbertp.springproject.entities.account.Account;
import fr.miage.kelbertp.springproject.entities.session.Session;
import fr.miage.kelbertp.springproject.utils.AccountLogin;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {ProjectApplication.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SessionTests {
    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;

    @BeforeAll
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    private byte[] getSession(int capacity, double price, String date) {
        Session session = new Session(capacity, price, date);

        String jsonStr = "";
        try {
            // Convert object to JSON string
            jsonStr = new ObjectMapper().writeValueAsString(session);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonStr.getBytes();
    }

    @Test
    @Order(1)
    void testPostSessionEndpoint() throws Exception {
        String token = AccountLogin.getToken("admin@test.com", "password", mockMvc);

        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("POST");
                    request.setRequestURI("/conferences/1/sessions");
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    request.addHeader("Authorization", token);
                    request.setContent(getSession(10, 10.0, "2021-01-01 00:00:00"));
                    return request;
                }).andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.capacity").value(10))
                .andExpect(jsonPath("$.price").value(10.0))
                .andExpect(jsonPath("$.date").value("2021-01-01 00:00:00"))
                .andExpect(jsonPath("$.conference").exists())
                .andExpect(jsonPath("$.conference.id").value(1))
                .andExpect(jsonPath("$._links.self.href").exists())
                .andExpect(jsonPath("$._links.collection.href").exists())
                .andExpect(jsonPath("$._links.register.href").exists());
    }

    @Test
    @Order(2)
    void testGetSessionEndpoint() throws Exception {
        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("GET");
                    request.setRequestURI("/conferences/1/sessions/1");
                    return request;
                }).andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.capacity").value(5))
                .andExpect(jsonPath("$.price").value(100.0))
                .andExpect(jsonPath("$.date").value("2023-05-01 10:00:00"))
                .andExpect(jsonPath("$.conference").exists())
                .andExpect(jsonPath("$.conference.id").value(1))
                .andExpect(jsonPath("$._links.self.href").exists())
                .andExpect(jsonPath("$._links.register.href").exists())
                .andExpect(jsonPath("$._links.collection.href").exists());
    }

    @Test
    @Order(3)
    void testGetAllSessionsForConference() throws Exception {
        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("GET");
                    request.setRequestURI("/conferences/1/sessions");
                    return request;
                }).andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.sessionList").exists())
                .andExpect(jsonPath("$._embedded.sessionList[0].id").value("1"))
                .andExpect(jsonPath("$._embedded.sessionList[0].capacity").value("5"))
                .andExpect(jsonPath("$._embedded.sessionList[0].price").value("100.0"))
                .andExpect(jsonPath("$._embedded.sessionList[0].date").value("2023-05-01 10:00:00"))
                .andExpect(jsonPath("$._embedded.sessionList[0].conference").exists())
                .andExpect(jsonPath("$._embedded.sessionList[0].conference.id").value("1"))
                .andExpect(jsonPath("$._embedded.sessionList[0]._links.self.href").exists())
                .andExpect(jsonPath("$._embedded.sessionList[0]._links.register.href").exists())
                .andExpect(jsonPath("$._embedded.sessionList[0]._links.collection.href").exists());
    }

    @Test
    @Order(4)
    void testPatchSession() throws Exception {
        String token = AccountLogin.getToken("admin@test.com", "password", mockMvc);

        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("PATCH");
                    request.setRequestURI("/conferences/1/sessions/1");
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    request.addHeader("Authorization", token);
                    request.setContent(getSession(6, 11.0, "2021-02-01 00:00:00"));
                    return request;
                }).andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.capacity").value(6))
                .andExpect(jsonPath("$.price").value(11.0))
                .andExpect(jsonPath("$.date").value("2021-02-01 00:00:00"))
                .andExpect(jsonPath("$.conference").exists())
                .andExpect(jsonPath("$.conference.id").value(1))
                .andExpect(jsonPath("$._links.self.href").exists())
                .andExpect(jsonPath("$._links.register.href").exists())
                .andExpect(jsonPath("$._links.collection.href").exists());
    }

    @Test
    void testPostSessionEndpointBadRequest() throws Exception {
        String token = AccountLogin.getToken("admin@test.com", "password", mockMvc);

        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("POST");
                    request.setRequestURI("/conferences/1/sessions");
                    request.addHeader("Authorization", token);
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    request.setContent(getSession(-5, 10.0, "2021-01-01 00:00:00"));
                    return request;
                }).andExpect(status().isBadRequest());
    }

    @Test
    void testPostSessionEndpointUnauthorized() throws Exception {
        String token = AccountLogin.getToken("user3@test.com", "password", mockMvc);

        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("POST");
                    request.setRequestURI("/conferences/1/sessions");
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    request.addHeader("Authorization", token);
                    request.setContent(getSession(10, 10.0, "2021-01-01 00:00:00"));
                    return request;
                }).andExpect(status().isUnauthorized());
    }



    @Test
    void testGetSessionConferenceNotFound() throws Exception {
        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("GET");
                    request.setRequestURI("/conferences/aaa/sessions/1");
                    return request;
                }).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Conference not found"));
    }

    @Test
    void testGetSessionNotFound() throws Exception {
        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("GET");
                    request.setRequestURI("/conferences/1/sessions/aaa");
                    return request;
                }).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Session not found"));
    }

    @Test
    void testGetSessionForbidden() throws Exception {
        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("GET");
                    request.setRequestURI("/conferences/1/sessions/5");
                    return request;
                }).andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message").value("You can't access to this resource"));
    }

    @Test
    void testGetAllSessionsForConferenceNotFound() throws Exception {
        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("GET");
                    request.setRequestURI("/conferences/aaa/sessions");
                    return request;
                }).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Conference not found"));
    }

    @Test
    void testPatchSessionEndpointUnauthorized() throws Exception {
        String token = AccountLogin.getToken("user3@test.com", "password", mockMvc);

        mockMvc.perform(servletContext -> {
            MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
            request.setMethod("PATCH");
            request.setRequestURI("/conferences/1/sessions/1");
            request.setContentType(MediaType.APPLICATION_JSON_VALUE);
            request.addHeader("Authorization", token);
            request.setContent(getSession(10, 10.0, "2021-01-01 00:00:00"));
            return request;
        }).andExpect(status().isUnauthorized());
    }

    @Test
    void testPatchSessionConferenceNotFound() throws Exception {
        String token = AccountLogin.getToken("admin@test.com", "password", mockMvc);

        mockMvc.perform(servletContext -> {
            MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
            request.setMethod("PATCH");
            request.setRequestURI("/conferences/aaa/sessions/1");
            request.setContentType(MediaType.APPLICATION_JSON_VALUE);
            request.addHeader("Authorization", token);
            request.setContent(getSession(10, 10.0, "2021-01-01 00:00:00"));
            return request;
        }).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Conference not found"));
    }

    @Test
    void testPatchSessionNotFound() throws Exception {
        String token = AccountLogin.getToken("admin@test.com", "password", mockMvc);

        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("PATCH");
                    request.setRequestURI("/conferences/1/sessions/aaaa");
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    request.addHeader("Authorization", token);
                    request.setContent(getSession(10, 10.0, "2021-01-01 00:00:00"));
                    return request;
                }).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Session not found"));
    }

    @Test
    void testPatchSessionForbidden() throws Exception {
        String token = AccountLogin.getToken("admin@test.com", "password", mockMvc);

        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("PATCH");
                    request.setRequestURI("/conferences/1/sessions/4");
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    request.addHeader("Authorization", token);
                    request.setContent(getSession(10, 10.0, "2021-01-01 00:00:00"));
                    return request;
                }).andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message").value("You can't access to this resource"));
    }


    @Test
    void testPatchSessionBadRequest() throws Exception {
        String token = AccountLogin.getToken("admin@test.com", "password", mockMvc);

        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("PATCH");
                    request.setRequestURI("/conferences/1/sessions/1");
                    request.addHeader("Authorization", token);
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    request.setContent(getSession(-5, 10.0, "2021-01-01 00:00:00"));
                    return request;
                }).andExpect(status().isBadRequest());
    }
}
