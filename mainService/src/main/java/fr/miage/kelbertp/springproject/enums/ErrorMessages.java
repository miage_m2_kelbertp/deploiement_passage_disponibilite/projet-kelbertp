package fr.miage.kelbertp.springproject.enums;

public class ErrorMessages {
    private ErrorMessages() {
    }

    public static final String ACCOUNT_NOT_FOUND = "{\"message\": \"Account not found\"}";
    public static final String BANK_UNAVAILABLE = "{ \"message\": \"Bank service unavailable\" }";
    public static final String CANT_ACCESS_TO_RESOURCE = "{\"message\": \"You can't access to this resource\"}";
    public static final String CONF_NOT_FOUND = "{\"message\": \"Conference not found\"}";
    public static final String REGISTER_ALREADY_PAID = "{\"message\": \"Register already paid\"}";
    public static final String REGISTER_NOT_FOUND = "{\"message\": \"Register not found\"}";
    public static final String SESSION_FULL = "{\"message\": \"Session is full\"}";
    public static final String SESSION_NOT_FOUND = "{\"message\": \"Session not found\"}";
    public static final String USER_NOT_ALLOWED = "{\"message\": \"You are not allowed to do this\"}";
}
