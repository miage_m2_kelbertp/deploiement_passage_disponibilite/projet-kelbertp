package fr.miage.kelbertp.springproject.entities.register;

import fr.miage.kelbertp.springproject.entities.account.Account;
import fr.miage.kelbertp.springproject.entities.session.Session;
import jakarta.persistence.*;
import lombok.*;

import java.sql.Timestamp;

@Entity
@Table(name = "register",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"session_id", "account_id"})
        }
)
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class Register {
    @Id
    private String id;
    @OneToOne
    @JoinColumn(name = "account_id")
    private Account account;
    @OneToOne
    @JoinColumn(name = "session_id")
    private Session session;
    private Timestamp paymentDate;

    public Register(Session session, Account account, Timestamp paymentDate) {
        this.session = session;
        this.account = account;
        this.paymentDate = paymentDate;
    }
}
