package fr.miage.kelbertp.springproject.control.assembler;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import fr.miage.kelbertp.springproject.boundary.user.AccountRepresentation;
import fr.miage.kelbertp.springproject.entities.account.Account;
import fr.miage.kelbertp.springproject.entities.account.validators.accountinput.AccountInput;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class AccountAssembler implements RepresentationModelAssembler<Account, EntityModel<Account>> {

    @Value("${jwt.secret}")
    private String secretToken;

    @Override
    @NonNull
    public EntityModel<Account> toModel(@NonNull Account account) {
        // get spring security token

        Algorithm algorithm = Algorithm.HMAC256(secretToken);
        String token = JWT.create()
                .withIssuer("auth0")
                .withClaim("id", account.getId())
                .withClaim("firstname", account.getFirstName())
                .withClaim("lastname", account.getLastName())
                .withClaim("email", account.getEmail())
                .withClaim("role", account.getRole())
                .withClaim("exp", System.currentTimeMillis() + 3600000)
                .sign(algorithm);
        account.setToken(token);
        return EntityModel.of(account,
                linkTo(methodOn(AccountRepresentation.class).getAccount(account.getId())).withSelfRel(),
                linkTo(methodOn(AccountRepresentation.class).getAllAccounts()).withRel("collection"),
                linkTo(methodOn(AccountRepresentation.class).getAllRegisters(null, account.getId())).withRel("registers"));
    }

    public EntityModel<Account> toModelWithoutToken(@NonNull Account account) {
        return EntityModel.of(account,
                linkTo(methodOn(AccountRepresentation.class).getAccount(account.getId())).withSelfRel(),
                linkTo(methodOn(AccountRepresentation.class).getAllAccounts()).withRel("collection"),
                linkTo(methodOn(AccountRepresentation.class).getAllRegisters(null, account.getId())).withRel("registers"));
    }

    @NonNull
    @Override
    public CollectionModel<EntityModel<Account>> toCollectionModel(@NonNull Iterable<? extends Account> accounts) {
        return CollectionModel.of(StreamSupport.stream(accounts.spliterator(), false)
                        .map(this::toModelWithoutToken)
                        .collect(Collectors.toList()),
                linkTo(methodOn(AccountRepresentation.class).getAllAccounts()).withSelfRel());
    }

    public Account toEntity(AccountInput account) {
        return new Account(UUID.randomUUID().toString(),
                account.getLastName(),
                account.getFirstName(),
                account.getEmail(),
                account.getPassword());
    }

}
