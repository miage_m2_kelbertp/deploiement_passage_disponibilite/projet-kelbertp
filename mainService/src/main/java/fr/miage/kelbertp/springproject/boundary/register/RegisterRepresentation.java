package fr.miage.kelbertp.springproject.boundary.register;

import fr.miage.kelbertp.springproject.boundary.conference.ConferenceResource;
import fr.miage.kelbertp.springproject.boundary.session.SessionResource;
import fr.miage.kelbertp.springproject.boundary.user.AccountResource;
import fr.miage.kelbertp.springproject.control.ExternalCall;
import fr.miage.kelbertp.springproject.control.JwtCheck;
import fr.miage.kelbertp.springproject.control.assembler.RegisterAssembler;
import fr.miage.kelbertp.springproject.entities.account.Account;
import fr.miage.kelbertp.springproject.entities.conference.Conference;
import fr.miage.kelbertp.springproject.entities.register.Register;
import fr.miage.kelbertp.springproject.entities.session.Session;
import fr.miage.kelbertp.springproject.enums.Roles;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static fr.miage.kelbertp.springproject.enums.ErrorMessages.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@ResponseBody
@RequestMapping(value = "/conferences/{idConference}/sessions/{idSession}/register", produces = "application/json")
public class RegisterRepresentation {

    private final RegisterResource rr;
    private final RegisterAssembler ra;
    private final ConferenceResource cr;
    private final SessionResource sr;
    private final AccountResource ar;
    private final RestTemplate restTemplate;

    public RegisterRepresentation(RegisterResource rr, RegisterAssembler ra, ConferenceResource cr, SessionResource sr, AccountResource ar, RestTemplate restTemplate) {
        this.rr = rr;
        this.ra = ra;
        this.cr = cr;
        this.sr = sr;
        this.ar = ar;
        this.restTemplate = restTemplate;
    }

    @PostMapping
    @Transactional
    public ResponseEntity<Object> createRegister(@RequestHeader("Authorization") String auth, @PathVariable String idConference, @PathVariable String idSession) {
        if (!JwtCheck.userCanAccess(auth, List.of(Roles.ROLE_ADMIN, Roles.ROLE_USER)))
            return ResponseEntity.status(401).build();

        Optional<Conference> conference = cr.findById(idConference);
        if (conference.isEmpty()) return ResponseEntity.status(404).body(CONF_NOT_FOUND);

        Optional<Session> session = sr.findById(idSession);
        if (session.isEmpty()) return ResponseEntity.status(404).body(SESSION_NOT_FOUND);

        if (!session.get().getConference().getId().equals(idConference))
            return ResponseEntity.status(403).body(CANT_ACCESS_TO_RESOURCE);

        if (rr.getNumberOfRegisterBySession(session.get()) >= session.get().getCapacity())
            return ResponseEntity.status(418).body(SESSION_FULL);

        Optional<Account> account = ar.findById(Objects.requireNonNull(JwtCheck.getId(auth)));
        if (account.isEmpty()) return ResponseEntity.status(404).body(ACCOUNT_NOT_FOUND);

        Register saved = rr.save(ra.toEntity(account.get(), session.get()));

        HashMap<String, String> pathVariables = new HashMap<>();
        pathVariables.put("idConference", idConference);
        pathVariables.put("idSession", idSession);

        URI location = linkTo(RegisterRepresentation.class, pathVariables).slash(saved.getId()).toUri();

        return ResponseEntity.created(location).body(ra.toModel(saved));
    }

    @PatchMapping("/{idRegister}")
    @Transactional
    public ResponseEntity<String> patchRegister(
            @RequestHeader("Authorization") String auth,
            @PathVariable String idConference,
            @PathVariable String idSession,
            @PathVariable String idRegister,
            @Value("${bank.url}") String bankUrl
    ) {
        if (!JwtCheck.userCanAccess(auth, List.of(Roles.ROLE_ADMIN, Roles.ROLE_USER)))
            return ResponseEntity.status(401).build();

        Optional<Conference> conference = cr.findById(idConference);
        if (conference.isEmpty()) return ResponseEntity.status(404).body(CONF_NOT_FOUND);

        Optional<Session> session = sr.findById(idSession);
        if (session.isEmpty()) return ResponseEntity.status(404).body(SESSION_NOT_FOUND);

        if (!session.get().getConference().getId().equals(idConference))
            return ResponseEntity.status(403).body(CANT_ACCESS_TO_RESOURCE);

        Optional<Register> register = rr.findById(idRegister);
        if (register.isEmpty()) return ResponseEntity.status(404).body(REGISTER_NOT_FOUND);

        if (!register.get().getSession().getId().equals(idSession))
            return ResponseEntity.status(403).body(CANT_ACCESS_TO_RESOURCE);

        if (!Objects.equals(JwtCheck.getId(auth), register.get().getAccount().getId()))
            return ResponseEntity.status(403).body(USER_NOT_ALLOWED);

        if (register.get().getPaymentDate() != null) return ResponseEntity.status(409).body(REGISTER_ALREADY_PAID);

        String url = bankUrl + "/banks/" + register.get().getAccount().getId();
        String json = "{\"amount\": -" + session.get().getPrice() + "}";

        ResponseEntity<String> response = ExternalCall.call(restTemplate, url, HttpMethod.POST, json);

        if (response.getStatusCode() == HttpStatus.OK) {
            register.get().setPaymentDate(new Timestamp(System.currentTimeMillis()));
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(402).body(response.getBody());
        }
    }
}
