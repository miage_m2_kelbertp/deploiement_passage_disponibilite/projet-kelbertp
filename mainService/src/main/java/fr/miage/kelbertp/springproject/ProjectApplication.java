package fr.miage.kelbertp.springproject;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ProjectApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProjectApplication.class, args);
    }

    @Bean
    public OpenAPI projetSpringApi() {
        return new OpenAPI()
                .info(new Info()
                        .title("Spring Project API - Kelbert Paul")
                        .version("1.0")
                        .description("Documentation Spring Project API v1.0."));
    }

    @Bean
    RestTemplate template() {
        return new RestTemplate();
    }
}
