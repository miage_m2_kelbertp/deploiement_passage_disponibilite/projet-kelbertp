package fr.miage.kelbertp.springproject.entities.account;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
@Table(name = "account", uniqueConstraints = {
        @UniqueConstraint(columnNames = "email")
})
public class Account {

    @Id
    private String id;
    private String lastName;
    private String firstName;
    private String email;
    @JsonIgnore
    private String password;
    @Transient
    private String token;
    private String role;

    public Account(String lastName, String firstName, String email, String password) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.password = password;
        this.role = "ROLE_USER";
    }

    public Account(String toString, String lastName, String firstName, String email, String password, String role) {
        this.id = toString;
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public Account(String id, String lastName, String firstName, String email, String password) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.password = password;
        this.role = "ROLE_USER";
    }
}
