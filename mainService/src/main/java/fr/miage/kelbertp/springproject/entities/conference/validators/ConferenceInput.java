package fr.miage.kelbertp.springproject.entities.conference.validators;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static fr.miage.kelbertp.springproject.exceptions.ValidationsMessages.PROPERTY_CANNOT_BE_BLANK;
import static fr.miage.kelbertp.springproject.exceptions.ValidationsMessages.PROPERTY_IS_REQUIRED;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConferenceInput {

    @NotNull(message = PROPERTY_IS_REQUIRED)
    @NotBlank(message = PROPERTY_CANNOT_BE_BLANK)
    private String name;
    @NotNull(message = PROPERTY_IS_REQUIRED)
    @NotBlank(message = PROPERTY_CANNOT_BE_BLANK)
    private String description;
    @NotNull(message = PROPERTY_IS_REQUIRED)
    @NotBlank(message = PROPERTY_CANNOT_BE_BLANK)
    private String speaker;
    @NotNull(message = PROPERTY_IS_REQUIRED)
    @NotBlank(message = PROPERTY_CANNOT_BE_BLANK)
    private String location;
}
