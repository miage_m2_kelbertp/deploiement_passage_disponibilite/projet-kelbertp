package fr.miage.kelbertp.springproject.entities.conference.validators;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class ConferenceValidator {
    private final Validator validator;

    ConferenceValidator(Validator validator) {
        this.validator = validator;
    }

    public void validate(ConferenceInput conference) {
        Set<ConstraintViolation<ConferenceInput>> violations = validator.validate(conference);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }

    public void validateProperties(ConferenceInput conference) throws ConstraintViolationException {
        if (conference.getName() != null) {
            this.validateProperty(conference, "name");
        }

        if (conference.getDescription() != null) {
            this.validateProperty(conference, "description");
        }

        if (conference.getSpeaker() != null) {
            this.validateProperty(conference, "speaker");
        }

        if (conference.getLocation() != null) {
            this.validateProperty(conference, "location");
        }
    }

    private void validateProperty(ConferenceInput conference, String property) {
        Set<ConstraintViolation<ConferenceInput>> violations = validator.validateProperty(conference, property);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }
}
