package fr.miage.kelbertp.bankservice.entities.bankaccount;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.*;

@Entity
@Table(name = "bank_account", uniqueConstraints = {
    @UniqueConstraint(columnNames = "idAccount")
})
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class BankAccount {

    @Id
    private String id;
    private String idAccount;
    private double pay;

    public BankAccount(String idAccount, double pay) {
        this.idAccount = idAccount;
        this.pay = pay;
    }
}
