package fr.miage.kelbertp.bankservice.entities.bankaccount.validator;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class AmountValidator {
    private final Validator validator;

    AmountValidator(Validator validator) {
        this.validator = validator;
    }

    public void validate(AmountInput amount) {
        Set<ConstraintViolation<AmountInput>> violations = validator.validate(amount);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }
}
