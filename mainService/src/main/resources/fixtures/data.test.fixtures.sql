INSERT INTO account (id, email, first_name, last_name, password, role)
VALUES
    (1, 'admin@test.com', 'John', 'Doe', 'password', 'ROLE_ADMIN'),
    (2, 'user1@test.com', 'Jane', 'Doe', 'password', 'ROLE_USER'),
    (3, 'user2@test.com', 'Bob', 'Smith', 'password', 'ROLE_USER'),
    (4, 'user3@test.com', 'Alice', 'Johnson', 'password', 'ROLE_USER'),
    (5, 'user4@test.com', 'Michael', 'Davis', 'password', 'ROLE_USER'),
    (6, 'user5@test.com', 'Linda', 'Wilson', 'password', 'ROLE_USER');

INSERT INTO conference (id, name, description, speaker, location)
VALUES
    (1, 'Conference 1', 'Description 1', 'Speaker 1', 'Location 1'),
    (2, 'Conference 2', 'Description 2', 'Speaker 2', 'Location 2'),
    (3, 'Conference 3', 'Description 3', 'Speaker 3', 'Location 3'),
    (4, 'Conference 4', 'Description 4', 'Speaker 4', 'Location 4');

INSERT INTO session (id, capacity, date, price, conference_id)
VALUES
    (1, 5, '2023-05-01 10:00:00', 100.00, '1'),
    (2, 3, '2023-05-02 14:00:00', 75.00, '1'),
    (3, 8, '2023-06-10 09:00:00', 120.00, '2'),
    (4, 4, '2023-06-11 13:00:00', 90.00, '2'),
    (5, 6, '2023-07-20 11:00:00', 80.00, '3'),
    (6, 5, '2023-07-21 15:00:00', 70.00, '3'),
    (7, 7, '2023-08-30 10:00:00', 110.00, '4'),
    (8, 2, '2023-08-31 14:00:00', 85.00, '4');

INSERT INTO register (id, account_id, session_id, payment_date)
VALUES
    (1, 2, 1, now()),
    (2, 3, 1, null),
    (3, 4, 2, now()),
    (4, 6, 2, null),
    (5, 1, 3, now()),
    (6, 3, 3, null),
    (7, 5, 4, now()),
    (8, 2, 5, null),
    (9, 4, 5, null),
    (10, 6, 6, now()),
    (11, 1, 7, null),
    (12, 2, 7, null),
    (13, 3, 8, now()),
    (14, 5, 8, null);
