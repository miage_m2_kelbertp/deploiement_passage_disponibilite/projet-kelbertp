package fr.miage.kelbertp.springproject;

import fr.miage.kelbertp.springproject.utils.AccountLogin;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {ProjectApplication.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class RegisterTest {

    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;

    @BeforeAll
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    @Order(1)
    void testRegister() throws Exception {
        String token = AccountLogin.getToken("user3@test.com", "password", mockMvc);

        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("POST");
                    request.setRequestURI("/conferences/1/sessions/1/register");
                    request.addHeader("Authorization", token);
                    return request;
                })
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.account.id").value("4"))
                .andExpect(jsonPath("$.session.id").value("1"))
                .andExpect(jsonPath("$.session.conference.id").value("1"))
                .andExpect(jsonPath("$._links.payment.href").exists())
                .andExpect(jsonPath("$._links.allRegisters.href").exists())
                .andExpect(jsonPath("$.payment_date").doesNotExist());
    }

    @Test
    @Order(2)
    void testRegisterConflict() throws Exception {
        String token = AccountLogin.getToken("user2@test.com", "password", mockMvc);

        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("POST");
                    request.setRequestURI("/conferences/1/sessions/1/register");
                    request.addHeader("Authorization", token);
                    return request;
                })
                .andExpect(status().isConflict());
    }

    @Test
    void testRegisterConferenceNotFound() throws Exception {
        String token = AccountLogin.getToken("user2@test.com", "password", mockMvc);

        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("POST");
                    request.setRequestURI("/conferences/aaa/sessions/1/register");
                    request.addHeader("Authorization", token);
                    return request;
                })
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Conference not found"));
    }

    @Test
    void testRegisterSessionNotFound() throws Exception {
        String token = AccountLogin.getToken("user2@test.com", "password", mockMvc);

        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("POST");
                    request.setRequestURI("/conferences/1/sessions/aaa/register");
                    request.addHeader("Authorization", token);
                    return request;
                })
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Session not found"));
    }

    @Test
    void testRegisterSessionNotInConference() throws Exception {
        String token = AccountLogin.getToken("user2@test.com", "password", mockMvc);

        mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
                    request.setMethod("POST");
                    request.setRequestURI("/conferences/1/sessions/3/register");
                    request.addHeader("Authorization", token);
                    return request;
                })
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message").value("You can't access to this resource"));
    }
}
