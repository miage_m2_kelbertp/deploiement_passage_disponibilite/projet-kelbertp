package fr.miage.kelbertp.springproject.boundary.register;

import fr.miage.kelbertp.springproject.entities.account.Account;
import fr.miage.kelbertp.springproject.entities.register.Register;
import fr.miage.kelbertp.springproject.entities.session.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RegisterResource extends JpaRepository<Register, String> {
    @Query("SELECT COUNT(r) FROM Register r WHERE r.session = :session")
    int getNumberOfRegisterBySession(@Param("session") Session session);

    @Query("SELECT r FROM Register r WHERE r.account = :account")
    Iterable<Register> getRegisterByAccount(@Param("account") Account account);
}
