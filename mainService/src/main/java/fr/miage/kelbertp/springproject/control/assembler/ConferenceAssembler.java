package fr.miage.kelbertp.springproject.control.assembler;

import fr.miage.kelbertp.springproject.boundary.conference.ConferenceRepresentation;
import fr.miage.kelbertp.springproject.boundary.session.SessionRepresentation;
import fr.miage.kelbertp.springproject.entities.conference.Conference;
import fr.miage.kelbertp.springproject.entities.conference.validators.ConferenceInput;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class ConferenceAssembler implements RepresentationModelAssembler<Conference, EntityModel<Conference>> {

    public Conference toEntity(ConferenceInput ci) {
        return new Conference(
                UUID.randomUUID().toString(),
                ci.getName(),
                ci.getDescription(),
                ci.getSpeaker(),
                ci.getLocation()
        );
    }

    @Override
    @NonNull
    public EntityModel<Conference> toModel(@NonNull Conference u) {
        return EntityModel.of(u,
                linkTo(methodOn(ConferenceRepresentation.class).getConference(u.getId())).withSelfRel(),
                linkTo(methodOn(ConferenceRepresentation.class).getConferences()).withRel("collection"),
                linkTo(methodOn(SessionRepresentation.class).getAllSessions(u.getId())).withRel("sessions")
        );
    }
}
