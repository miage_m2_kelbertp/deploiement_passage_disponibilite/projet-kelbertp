package fr.miage.kelbertp.springproject.entities.conference;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
@Table(name = "conference")
public class Conference {

    @Id
    private String id;
    private String name;
    private String description;
    private String speaker;
    private String location;

    public Conference(String name, String description, String speaker, String location) {
        this.name = name;
        this.description = description;
        this.speaker = speaker;
        this.location = location;
    }


}
