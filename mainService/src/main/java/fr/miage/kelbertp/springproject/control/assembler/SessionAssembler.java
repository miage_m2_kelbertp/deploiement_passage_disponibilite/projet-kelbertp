package fr.miage.kelbertp.springproject.control.assembler;

import fr.miage.kelbertp.springproject.boundary.register.RegisterRepresentation;
import fr.miage.kelbertp.springproject.boundary.session.SessionRepresentation;
import fr.miage.kelbertp.springproject.entities.conference.Conference;
import fr.miage.kelbertp.springproject.entities.session.Session;
import fr.miage.kelbertp.springproject.entities.session.validator.SessionInput;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class SessionAssembler implements RepresentationModelAssembler<Session, EntityModel<Session>> {

    public Session toEntity(SessionInput si, Conference c) {
        return new Session(
                UUID.randomUUID().toString(),
                c,
                si.getCapacity(),
                si.getPrice(),
                si.getDate()
        );
    }

    @Override
    public EntityModel<Session> toModel(@NonNull Session session) {
        return EntityModel.of(session,
                linkTo(methodOn(SessionRepresentation.class).getSession(
                        session.getConference().getId(),
                        session.getId()
                )).withSelfRel(),
                linkTo(methodOn(SessionRepresentation.class).getAllSessions(
                        session.getConference().getId()
                )).withRel("collection"),
                linkTo(methodOn(RegisterRepresentation.class).createRegister(
                        null,
                        session.getConference().getId(),
                        session.getId()
                )).withRel("register"));
    }
}
