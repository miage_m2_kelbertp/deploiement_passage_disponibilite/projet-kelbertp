package fr.miage.kelbertp.springproject.entities.session.validator;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static fr.miage.kelbertp.springproject.exceptions.ValidationsMessages.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SessionInput {
    @Min(value = 1, message = MIN_CAPACITY)
    private int capacity;
    @Min(value = 1, message = MIN_CAPACITY_PRICE)
    private double price;
    @NotNull(message = PROPERTY_IS_REQUIRED)
    @Pattern(regexp = "^(\\d{4})-(\\d{2})-(\\d{2}) (\\d{2}):(\\d{2}):(\\d{2})$", message = INVALID_DATE_FORMAT)
    private String date;
}
