package fr.miage.kelbertp.bankservice.entities.bankaccount.validator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AmountInput {
    @NotNull(message = "Amount is required")
    private double amount;
}
