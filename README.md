# Projet Conférence

<p>
<a href="https://gitlab.com/miage_m2_kelbertp/deploiement_passage_disponibilite/projet-kelbertp" target="_blank"><img src="https://img.shields.io/badge/repo-gitlab-orange.svg?style=for-the-badge&logo=gitlab"/></a> 
<a href="https://gitlab.com/miage_m2_kelbertp/deploiement_passage_disponibilite/projet-kelbertp/-/pipelines" target="_blank"><img src="https://img.shields.io/badge/build-passing-green.svg?style=for-the-badge&logo=docker"/></a>
<a href="https://www.postman.com/cloudy-spaceship-261933/workspace/projet-spring-kelbertp/collection/9275228-cff93ed0-dd25-4b42-8699-74724a2d3fb4?action=share&creator=9275228" target="_blank"><img src="https://img.shields.io/badge/test-postman-lightgrey.svg?style=for-the-badge&logo=postman"/></a>
</p>

###### *Kelbert Paul - MIAGE M2 SID*

## Sommaire

- [Description](#description)
- [Technologies](#technologies)
- [Lancement](#lancement)
- [Tester](#tester)
- [Fonctionnement](#fonctionnement)
- [Plus tard](#plus-tard)
- [Pour participer](#pour-participer)
- [Licence](#licence)

---

## Description

**Proposer une API permettant la gestion d'un système de conférences.**

Un utilistateur **doit** pouvoir :

- S'inscrire
- Consulter les conférences et les sessions disponibles
- Réserver sa ou ses places (et les payer)
- Gérer sa banque (accès à son compte, ajouter/retirer de l'argent)

Un administrateur **doit** pouvoir, en plus des droits utilisateurs.

Attention, le service bancaire n'est pas une vraie banque, il s'agit d'un portefeuille bancaire.

## Technologies

<div align="center" width=150">   <img src="https://spring.io/img/spring-2.svg" alt="Spring logo" height="80">   <img src="https://logos-marques.com/wp-content/uploads/2021/03/Java-Logo.png" alt="Java logo" height="80"> </div>  <div align="center" width=150"><img src="https://www.docker.com/wp-content/uploads/2022/03/horizontal-logo-monochromatic-white.png" alt="Docker logo" height="80">   <img src="https://wiki.postgresql.org/images/3/30/PostgreSQL_logo.3colors.120x120.png" alt="PostgreSQL logo" height="80">   <img src="https://www.h2database.com/html/images/h2-logo-2.png" alt="H2 logo" height="80"> <img src="https://voyager.postman.com/logo/postman-logo-icon-orange.svg" alt="Postman" height="80"> </div>

## Lancement

*L'API principale sera disponible à l'adresse `http://localhost:3000`*

#### Via Docker

```
git pull https://gitlab.com/miage_m2_kelbertp/deploiement_passage_disponibilite/projet-kelbertp.git

docker compose up -d
```

#### Manuellement

Le fichier de configuration se trouve dans `src/main/resources/application.properties`

```
git pull https://gitlab.com/miage_m2_kelbertp/deploiement_passage_disponibilite/projet-kelbertp.git
cd mainService
mvn clean package
java -jar target/springProject-0.0.1-SNAPSHOT.jar
```

*Penser à modifier dans le mainService le fichier `applications.properties`afin de connecter correctement la bonne base
de données.*

Ouvrir un autre terminal

```
cd bankService
mvn clean package
java -jar target/bankService-0.0.1-SNAPSHOT.jar
```

## Tester

Afin de tester, deux possibilités existent :

- Récupérer le postman
  à [ce lien](https://www.postman.com/cloudy-spaceship-261933/workspace/projet-spring-kelbertp/collection/9275228-cff93ed0-dd25-4b42-8699-74724a2d3fb4?action=share&creator=9275228)
- Ou alors téléhcarger le json à importer dans postman dans le
  dossier `./postman/Projet Kelbert Paul.postman_collection.json

Dans les deux cas, il est conseillé de sélectionner l'environnement `Projet DPD` en haut à droite

## Fonctionnement

Globalement, l'application présente deux services : **main** (authentification, gestion des
conférences/sessions/inscriptions) et **bank** (gestion de l'argent des utilisateurs).

Chacun de ces services communiquent entre eux, avec différentes bases de données.

L'ensemble peut être modélisé comme suit :

<div align="center"><img src="./assets/deployment_diagram.png"></div>

## Plus tard

Il manque encore quelques fonctionnalités à l'application.

~~Par exemple, aujourd'hui la CI n'exécute que les tests présents sur le mainService.~~ (Fait)

Il faudrait aussi mettre en place un load balancer pour le service bancaire, et un circuit breaker.

Il manque également les contrôles de cohérence ainsi que des contrôles de sécurité (un utilisateur ne doit pas pouvoir
accès à un autre), qu'il faudrait ajouter assez rapidement.

Un dernier ajout très important est d'implémenter le fait que si l'application est lancée sans base de données, il ne
faudrait pas que tout explose. Dans le cas où la base est coupée durant l'exécution du programme, ce cas est géré.

## Pour participer

Si vous souhaitez contribuer à ce projet, voici les étapes à suivre :

1. Forkez ce repo
2. Créez une nouvelle branche: git checkout -b ma_nouvelle_fonctionnalité
3. Effectuez vos modifications
4. Faites un commit de vos modifications: git commit -m "Ajout de ma nouvelle fonctionnalité"
5. Push votre branche: git push origin ma_nouvelle_fonctionnalité
6. Créez une Pull Request

## Licence

Ce projet est sous une licence [MIT](https://fr.wikipedia.org/wiki/Massachusetts_Institute_of_Technology).
