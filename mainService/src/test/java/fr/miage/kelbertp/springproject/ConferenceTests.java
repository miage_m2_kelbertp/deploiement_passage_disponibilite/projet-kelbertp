package fr.miage.kelbertp.springproject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.miage.kelbertp.springproject.entities.conference.Conference;
import fr.miage.kelbertp.springproject.utils.AccountLogin;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {ProjectApplication.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ConferenceTests {
    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;

    @BeforeAll
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    private byte[] getConference(String name, String desc, String location, String speaker) {
        Conference conference = new Conference(name, desc, speaker, location);
        String jsonStr = "";
        try {
            // Convert object to JSON string
            jsonStr = new ObjectMapper().writeValueAsString(conference);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonStr.getBytes();
    }

    @Test
    @Order(1)
    void testPostConferenceEndpoint() throws Exception {
        String token = AccountLogin.getToken("admin@test.com", "password", mockMvc);
        this.mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest();
                    request.setMethod("POST");
                    request.setRequestURI("/conferences");
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    request.addHeader("Authorization", token);
                    request.setContent(this.getConference("TestConf", "TestConfDesc", "TestConfLocation", "TestConfSpeaker"));
                    return request;
                }).andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("TestConf"))
                .andExpect(jsonPath("$.description").value("TestConfDesc"))
                .andExpect(jsonPath("$.location").value("TestConfLocation"))
                .andExpect(jsonPath("$.speaker").value("TestConfSpeaker"))
                .andExpect(jsonPath("$._links.self.href").exists())
                .andExpect(jsonPath("$._links.sessions.href").exists())
                .andExpect(jsonPath("$._links.collection.href").exists());
    }

    @Test
    @Order(2)
    void testGetConference() throws Exception {
        this.mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest();
                    request.setMethod("GET");
                    request.setRequestURI("/conferences/1");
                    return request;
                }).andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Conference 1"))
                .andExpect(jsonPath("$.description").value("Description 1"))
                .andExpect(jsonPath("$.location").value("Location 1"))
                .andExpect(jsonPath("$.speaker").value("Speaker 1"))
                .andExpect(jsonPath("$._links.self.href").exists())
                .andExpect(jsonPath("$._links.sessions.href").exists())
                .andExpect(jsonPath("$._links.collection.href").exists());
    }

    @Test
    @Order(3)
    void testGetAllConferences() throws Exception {
        this.mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest();
                    request.setMethod("GET");
                    request.setRequestURI("/conferences");
                    return request;
                }).andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.conferenceList[0].name").value("Conference 1"))
                .andExpect(jsonPath("$._embedded.conferenceList[0].description").value("Description 1"))
                .andExpect(jsonPath("$._embedded.conferenceList[0].location").value("Location 1"))
                .andExpect(jsonPath("$._embedded.conferenceList[0].speaker").value("Speaker 1"))
                .andExpect(jsonPath("$._embedded.conferenceList[0]._links.self.href").exists())
                .andExpect(jsonPath("$._embedded.conferenceList[0]._links.sessions.href").exists())
                .andExpect(jsonPath("$._embedded.conferenceList[0]._links.collection.href").exists());
    }

    @Test
    @Order(4)
    void testGetConferenceBySearch() throws Exception {
        this.mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest();
                    request.setMethod("GET");
                    request.setRequestURI("/conferences/search");
                    request.addParameter("data", "Conference 1");
                    return request;
                }).andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.conferenceList[0].name").value("Conference 1"))
                .andExpect(jsonPath("$._embedded.conferenceList[0].description").value("Description 1"))
                .andExpect(jsonPath("$._embedded.conferenceList[0].location").value("Location 1"))
                .andExpect(jsonPath("$._embedded.conferenceList[0].speaker").value("Speaker 1"))
                .andExpect(jsonPath("$._embedded.conferenceList[0]._links.self.href").exists())
                .andExpect(jsonPath("$._embedded.conferenceList[0]._links.sessions.href").exists())
                .andExpect(jsonPath("$._embedded.conferenceList[0]._links.collection.href").exists());
    }

    @Test
    @Order(5)
    void testPatchConferenceEndpoint() throws Exception {
        String token = AccountLogin.getToken("admin@test.com", "password", mockMvc);
        this.mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest();
                    request.setMethod("PATCH");
                    request.setRequestURI("/conferences/1");
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    request.addHeader("Authorization", token);
                    request.setContent(this.getConference("TestConfEdit", "TestConfDescEdit", "TestConfLocationEdit", "TestConfSpeakerEdit"));
                    return request;
                }).andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("TestConfEdit"))
                .andExpect(jsonPath("$.description").value("TestConfDescEdit"))
                .andExpect(jsonPath("$.location").value("TestConfLocationEdit"))
                .andExpect(jsonPath("$.speaker").value("TestConfSpeakerEdit"))
                .andExpect(jsonPath("$._links.self.href").exists())
                .andExpect(jsonPath("$._links.sessions.href").exists())
                .andExpect(jsonPath("$._links.collection.href").exists());
    }

    @Test
    void testPostConferenceEndpointUnauthorized() throws Exception {
        this.mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest();
                    request.setMethod("POST");
                    request.setRequestURI("/conferences");
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    request.setContent(this.getConference("TestConf", "TestConfDesc", "TestConfLocation", "TestConfSpeaker"));
                    return request;
                }).andExpect(status().isUnauthorized());
    }

    @Test
    void testGetConferenceNotFound() throws Exception {
        String token = AccountLogin.getToken("admin@test.com", "password", mockMvc);
        this.mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest();
                    request.setMethod("GET");
                    request.addHeader("Authorization", token);
                    request.setRequestURI("/conferences/zzzzz");
                    return request;
                }).andExpect(status().isNotFound());
    }

    @Test
    void testGetConferenceBySearchBadRequest() throws Exception {
        this.mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest();
                    request.setMethod("GET");
                    request.setRequestURI("/conferences/search");
                    return request;
                }).andExpect(status().isBadRequest());
    }

    @Test
    void testGetConferenceBySearchBadRequestBlank() throws Exception {
        this.mockMvc.perform(servletContext -> {
            MockHttpServletRequest request = new MockHttpServletRequest();
            request.setMethod("GET");
            request.setRequestURI("/conferences/search");
            request.addParameter("data", "");
            return request;
        }).andExpect(status().isBadRequest());
    }

    @Test
    void testPatchConferenceUnauthorized() throws Exception {
        this.mockMvc.perform(servletContext -> {
            MockHttpServletRequest request = new MockHttpServletRequest();
            request.setMethod("PATCH");
            request.setRequestURI("/conferences/1");
            request.setContentType(MediaType.APPLICATION_JSON_VALUE);
            request.setContent(this.getConference("TestConf", "TestConfDesc", "TestConfLocation", "TestConfSpeaker"));
            return request;
        }).andExpect(status().isUnauthorized());
    }

    @Test
    void testPatchConferenceNotFound() throws Exception {
        String token = AccountLogin.getToken("admin@test.com", "password", mockMvc);
        this.mockMvc.perform(servletContext -> {
            MockHttpServletRequest request = new MockHttpServletRequest();
            request.setMethod("PATCH");
            request.setRequestURI("/conferences/aaaaaaa");
            request.addHeader("Authorization", token);
            request.setContentType(MediaType.APPLICATION_JSON_VALUE);
            request.setContent(this.getConference("TestConf", "TestConfDesc", "TestConfLocation", "TestConfSpeaker"));
            return request;
        }).andExpect(status().isNotFound());
    }

    @Test
    void testPatchConferenceBadRequest() throws Exception {
        String token = AccountLogin.getToken("admin@test.com", "password", mockMvc);
        this.mockMvc.perform(servletContext -> {
            MockHttpServletRequest request = new MockHttpServletRequest();
            request.setMethod("PATCH");
            request.setRequestURI("/conferences/1");
            request.addHeader("Authorization", token);
            request.setContentType(MediaType.APPLICATION_JSON_VALUE);
            request.setContent(this.getConference("TestConf", "TestConfDesc", "", ""));
            return request;
        }).andExpect(status().isBadRequest());
    }

}
