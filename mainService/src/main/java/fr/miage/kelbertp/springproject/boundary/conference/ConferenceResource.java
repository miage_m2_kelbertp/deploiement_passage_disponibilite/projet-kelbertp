package fr.miage.kelbertp.springproject.boundary.conference;

import fr.miage.kelbertp.springproject.entities.conference.Conference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ConferenceResource extends JpaRepository<Conference, String> {
    @Query("SELECT c FROM Conference c WHERE upper(c.name) LIKE %:data% or upper(c.description) LIKE %:data% or upper(c.location) LIKE %:data% or upper(c.speaker) LIKE %:data%")
    Iterable<Conference> searchConference(@Param("data") String data);
}
