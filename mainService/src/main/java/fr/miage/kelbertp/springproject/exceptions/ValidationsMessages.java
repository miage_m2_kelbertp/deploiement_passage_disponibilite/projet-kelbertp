package fr.miage.kelbertp.springproject.exceptions;

public class ValidationsMessages {
    private ValidationsMessages() {
    }

    public static final String EMAIL_MUST_BE_VALID = "Email must be valid";
    public static final String INVALID_DATE_FORMAT = "Date must be in format YYYY-MM-DD";
    public static final String MIN_CAPACITY = "Capacity must be at least 1";
    public static final String MIN_CAPACITY_PRICE = "Price must be at least 1";
    public static final String PASSWORD_MUST_BE_AT_LEAST_8_CHARACTERS_LONG = "Password must be at least 8 characters long";
    public static final String PROPERTY_CANNOT_BE_BLANK = "Property cannot be blank";
    public static final String PROPERTY_IS_REQUIRED = "Property is required";
}
