package fr.miage.kelbertp.bankservice.control;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.logging.Logger;

@Component
public class MainServiceInterceptor implements HandlerInterceptor {
    private static final Logger LOGGER = Logger.getLogger(MainServiceInterceptor.class.getName());

    @Override
    public boolean preHandle(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull Object handler) throws Exception {
        this.logPreRequest(request);

        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull Object handler, ModelAndView modelAndView) {
        this.logPostRequest(response);
    }

    private void logPreRequest(@NonNull HttpServletRequest request) {
        String msg = "[" + new Date() + "] : " + request.getMethod() + " " + request.getRequestURI();
        LOGGER.info(msg);
    }

    private void logPostRequest(@NonNull HttpServletResponse response) {
        String msg = "[" + new Date() + "] : Response " + response.getStatus();
        LOGGER.info(msg);
    }
}
