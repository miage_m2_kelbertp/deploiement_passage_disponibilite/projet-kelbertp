package fr.miage.kelbertp.bankservice.boundary;

import fr.miage.kelbertp.bankservice.entities.bankaccount.BankAccount;
import fr.miage.kelbertp.bankservice.entities.bankaccount.validator.AmountInput;
import jakarta.validation.Valid;
import jakarta.transaction.Transactional;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static fr.miage.kelbertp.bankservice.enums.ErrorMessages.BANK_ACCOUNT_NOT_FOUND;
import static fr.miage.kelbertp.bankservice.enums.ErrorMessages.NO_ENOUGH_MONEY;

@RestController
@ResponseBody
@RequestMapping(value = "/banks/{idAccount}", produces = "application/json")
public class BankRepresentation {
    private final BankResource br;

    private static final String PAY = "{ \"pay\": ";

    public BankRepresentation(BankResource br) {
        this.br = br;
    }

    @PostMapping("/create")
    @Transactional
    public ResponseEntity<String> createBankAccount(@PathVariable("idAccount") String idAccount) {

        if (idAccount == null) return ResponseEntity.status(400).body("idAccount is null");
        BankAccount bankAccount = new BankAccount(UUID.randomUUID().toString(), idAccount, 0);

        br.save(bankAccount);

        return ResponseEntity.ok().body(PAY + bankAccount.getPay() + " }");
    }

    @PostMapping
    @Transactional
    public ResponseEntity<String> updateBankAccount(@PathVariable("idAccount") String idAccount, @RequestBody @Valid AmountInput amountInput) {
        BankAccount bankAccount = br.findByIdAccount(idAccount);

        if (bankAccount == null) return ResponseEntity.status(404).body(BANK_ACCOUNT_NOT_FOUND);

        if (bankAccount.getPay() + amountInput.getAmount() < 0) return ResponseEntity.status(409).body(NO_ENOUGH_MONEY);

        bankAccount.setPay(bankAccount.getPay() + amountInput.getAmount());

        return ResponseEntity.ok().body(PAY + bankAccount.getPay() + " }");
    }

    @GetMapping
    public ResponseEntity<String> getBankAccount(@PathVariable("idAccount") String idAccount) {
        BankAccount bankAccount = br.findByIdAccount(idAccount);

        if (bankAccount == null) return ResponseEntity.status(404).body(BANK_ACCOUNT_NOT_FOUND);

        return ResponseEntity.ok().body(PAY + bankAccount.getPay() + " }");
    }
}
