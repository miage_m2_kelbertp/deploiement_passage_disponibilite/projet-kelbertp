package fr.miage.kelbertp.springproject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.miage.kelbertp.springproject.entities.account.Account;
import fr.miage.kelbertp.springproject.utils.AccountLogin;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {ProjectApplication.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AccountTests {
    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;
    private final String mailTest = "mail.test@test.fr";

    @BeforeAll
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    private byte[] getAccount(String lastName, String firstName, String email, String password) {
        Account account = new Account(lastName, firstName, email, password);
        String jsonStr = "";
        try {
            // Convert object to JSON string
            jsonStr = new ObjectMapper().writeValueAsString(account);
            // add password to json
            jsonStr = jsonStr.substring(0, jsonStr.length() - 1) + ",\"password\":\"" + password + "\"}";

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonStr.getBytes();
    }

    @Test
    @Order(2)
    void testGetAccount() throws Exception {
        this.mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest();
                    request.setMethod("GET");
                    request.setRequestURI("/accounts/" + 1);
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    return request;
                }).andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.lastName").value("Doe"))
                .andExpect(jsonPath("$.firstName").value("John"))
                .andExpect(jsonPath("$.email").value("admin@test.com"))
                .andExpect(jsonPath("$.password").doesNotExist())
                .andExpect(jsonPath("$.token").doesNotExist())
                .andExpect(jsonPath("$._links.self.href").exists())
                .andExpect(jsonPath("$._links.collection.href").exists())
                .andExpect(jsonPath("$._links.registers.href").exists());
    }

    @Order(4)
    @Test
    void testPatchAccount() throws Exception {
        this.mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest();
                    request.setMethod("PATCH");
                    request.setRequestURI("/accounts/" + 2);
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    request.setContent(this.getAccount("Tata", "Titi", "test-user1@test.com", "password"));
                    return request;
                }).andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(2))
                .andExpect(jsonPath("$.lastName").value("Tata"))
                .andExpect(jsonPath("$.firstName").value("Titi"))
                .andExpect(jsonPath("$.email").value("test-user1@test.com"))
                .andExpect(jsonPath("$.password").doesNotExist())
                .andExpect(jsonPath("$.token").exists())
                .andExpect(jsonPath("$._links.self.href").exists())
                .andExpect(jsonPath("$._links.collection.href").exists());
    }

    @Test
    @Order(5)
    void testPostLogin() throws Exception {
        this.mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest();
                    request.setMethod("POST");
                    request.setRequestURI("/accounts/login");
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    try {
                        request.setContent(new ObjectMapper().writeValueAsBytes(new AccountLogin("user5@test.com", "password")));
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                    return request;
                }).andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(6))
                .andExpect(jsonPath("$.lastName").value("Wilson"))
                .andExpect(jsonPath("$.firstName").value("Linda"))
                .andExpect(jsonPath("$.email").value("user5@test.com"))
                .andExpect(jsonPath("$.password").doesNotExist())
                .andExpect(jsonPath("$.token").exists())
                .andExpect(jsonPath("$._links.self.href").exists())
                .andExpect(jsonPath("$._links.collection.href").exists());
    }

    @Test
    @Order(6)
    void testGelAllAccounts() throws Exception {
        this.mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest();
                    request.setMethod("GET");
                    request.setRequestURI("/accounts");
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    return request;
                }).andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.accountList").exists())
                .andExpect(jsonPath("$._embedded.accountList[0].id").value(1))
                .andExpect(jsonPath("$._embedded.accountList[0].lastName").value("Doe"))
                .andExpect(jsonPath("$._embedded.accountList[0].firstName").value("John"))
                .andExpect(jsonPath("$._embedded.accountList[0].email").value("admin@test.com"))
                .andExpect(jsonPath("$._embedded.accountList[0].password").doesNotExist())
                .andExpect(jsonPath("$._embedded.accountList[0].token").doesNotExist())
                .andExpect(jsonPath("$._embedded.accountList[0]._links.self.href").exists())
                .andExpect(jsonPath("$._embedded.accountList[0]._links.collection.href").exists())
                .andExpect(jsonPath("$._embedded.accountList[0]._links.registers.href").exists())
                .andExpect(jsonPath("$._links.self.href").exists());
    }

    @Test
    @Order(7)
    void testGetAllRegisters() throws Exception {
        String token = AccountLogin.getToken("admin@test.com", "password", mockMvc);

        this.mockMvc.perform(servletContext -> {
                    MockHttpServletRequest request = new MockHttpServletRequest();
                    request.setMethod("GET");
                    request.setRequestURI("/accounts/1/registers");
                    request.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    request.addHeader("Authorization", token);
                    return request;
                }).andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.registerList").exists())
                .andExpect(jsonPath("$._embedded.registerList[0].id").value(5))
                .andExpect(jsonPath("$._embedded.registerList[0]._links.payment.href").exists())
                .andExpect(jsonPath("$._embedded.registerList[0]._links.allRegisters.href").exists());
    }

    @Test
    void testLoginUnauthorized() throws Exception {
        this.mockMvc.perform(servletContext -> {
            MockHttpServletRequest request = new MockHttpServletRequest();
            request.setMethod("POST");
            request.setRequestURI("/accounts/login");
            request.setContentType(MediaType.APPLICATION_JSON_VALUE);
            try {
                request.setContent(new ObjectMapper().writeValueAsBytes(new AccountLogin(this.mailTest, "password2")));
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
            return request;
        }).andExpect(status().isUnauthorized());
    }

    @Test
    void testGetAccountNotFound() throws Exception {
        this.mockMvc.perform(servletContext -> {
            MockHttpServletRequest request = new MockHttpServletRequest();
            request.setMethod("GET");
            request.setRequestURI("/accounts/" + "abc");
            request.setContentType(MediaType.APPLICATION_JSON_VALUE);
            return request;
        }).andExpect(status().isNotFound());
    }

    @Test
    void testPatchBadRequest() throws Exception {
        this.mockMvc.perform(servletContext -> {
            MockHttpServletRequest request = new MockHttpServletRequest();
            request.setMethod("PATCH");
            request.setRequestURI("/accounts/" + 2);
            request.setContentType(MediaType.APPLICATION_JSON_VALUE);
            request.setContent(this.getAccount("Tata", "Titi", mailTest, "pa"));
            return request;
        }).andExpect(status().isBadRequest());
    }

    @Test
    void testPatchNotFound() throws Exception {
        this.mockMvc.perform(servletContext -> {
            MockHttpServletRequest request = new MockHttpServletRequest();
            request.setMethod("PATCH");
            request.setRequestURI("/accounts/" + "abc");
            request.setContentType(MediaType.APPLICATION_JSON_VALUE);
            request.setContent(this.getAccount("Tata", "Titi", mailTest, "password"));
            return request;
        }).andExpect(status().isNotFound());
    }
}
