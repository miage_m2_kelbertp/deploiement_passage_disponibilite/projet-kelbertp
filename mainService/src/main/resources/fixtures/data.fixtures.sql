-- Insérer les données de compte
INSERT INTO account (id, email, first_name, last_name, password, role)
VALUES ('92d0a978-538c-4c9b-a27e-3c906f1d319e', 'admin@conference.com', 'Jean', 'Dupont', 'password', 'ROLE_ADMIN'),
       ('e1e88129-6b02-41b8-8e79-9e42760f311d', 'marie.dupont@conference.com', 'Marie', 'Dupont', 'password', 'ROLE_USER'),
       ('8cfa5a6b-ba5b-4e0c-8b2f-90ebfb6e4a4b', 'luc.martin@conference.com', 'Luc', 'Martin', 'password', 'ROLE_USER'),
       ('3dc7f334-3d2a-46b1-9b9f-79a6f5bb7c96', 'emilie.lefebvre@conference.com', 'Emilie', 'Lefebvre', 'password', 'ROLE_USER'),
       ('7fc3e3d2-7e75-4b9f-9dfe-61e5567fcf10', 'pierre.rousseau@conference.com', 'Pierre', 'Rousseau', 'password', 'ROLE_USER'),
       ('b02506dd-5a5b-4e4b-a747-6abaf4d90d4c', 'sophie.girard@conference.com', 'Sophie', 'Girard', 'password', 'ROLE_USER');


-- Insérer les données de conférence
INSERT INTO conference (id, description, location, name, speaker)
VALUES ('8b2f1f72-013d-4210-bb04-ecfa22f510f1', 'Annual Tech Conference', 'San Francisco, CA', 'TechConf 2023', 'John Smith'),
       ('582c25bf-cdf0-41d3-a662-52d42b24e8b9', 'Marketing Conference', 'New York, NY', 'MktgConf 2023', 'Jane Doe'),
       ('3d2d7f10-0cfc-44a6-86a7-20e8a32a9f9b', 'Industry Summit', 'London, UK', 'Summit 2023', 'David Lee'),
       ('7d0a1c8e-32de-441d-bf78-94a8aa01f27a', 'Startup Expo', 'Berlin, Germany', 'Startup Expo 2023', 'Alex Chen'),
       ('3b1e16d6-75b7-4ea3-8693-1b2ce8f1e9dc', 'Business Conference', 'Toronto, Canada', 'BizConf 2023', 'Emily Wong');


-- Insérer les données de session correspondantes
INSERT
INTO session (id, capacity, date, price, conference_id)
VALUES
    -- Sessions pour TechConf 2023
    ('20a7ed6f-0ec3-4523-98c6-c6e0e6f02984', 5, '2023-06-01 10:00:00', 200.00, (SELECT id FROM conference WHERE name = 'TechConf 2023')),
    ('d0a8b0f7-6a22-40a9-b2c1-6eaf2c0081e6', 3, '2023-06-02 14:00:00', 175.00, (SELECT id FROM conference WHERE name = 'TechConf 2023')),

    -- Sessions pour MktgConf 2023
    ('a8e93b50-8260-4b3c-955d-8e3464a5c4bb', 4, '2023-07-10 09:00:00', 150.00, (SELECT id FROM conference WHERE name = 'MktgConf 2023')),
    ('e4c4a5b5-2e5c-4ed9-bdcb-88b7bfcecc98', 6, '2023-07-11 13:00:00', 125.00, (SELECT id FROM conference WHERE name = 'MktgConf 2023')),

    -- Sessions pour Summit 2023
    ('7c2a1b29-422f-4b9e-9d0c-f0a7a466155c', 4, '2023-08-20 11:00:00', 175.00, (SELECT id FROM conference WHERE name = 'Summit 2023')),
    ('5e2f8a72-1de5-4b62-a845-8d24e4dd4f79', 9, '2023-08-21 15:00:00', 150.00, (SELECT id FROM conference WHERE name = 'Summit 2023')),

    -- Sessions pour Startup Expo 2023
    ('7a3564cf-6185-4b5c-a0e9-52a8d54da792', 10, '2023-09-30 10:00:00', 225.00, (SELECT id FROM conference WHERE name = 'Startup Expo 2023')),
    ('1be23d08-8f8b-448a-aead-0173b9e78c05', 10, '2023-10-01 14:00:00', 200.00, (SELECT id FROM conference WHERE name = 'Startup Expo 2023')),

    -- Sessions pour BizConf 2023
    ('f13f86c5-f84f-44c8-a6aa-c59cfae36435', 6, '2023-11-10 09:00:00', 175.00, (SELECT id FROM conference WHERE name = 'BizConf 2023')),
    ('2d8cc545-19e5-4711-b2aa-14be35a5fbc1', 2, '2023-11-11 13:00:00', 150.00, (SELECT id FROM conference WHERE name = 'BizConf 2023'));


-- Insérer les données de réservation
INSERT INTO register (id, account_id, session_id, payment_date)
VALUES
    -- Réservations pour TechConf 2023
    ('3198718b-b9d4-4c4b-af4f-4a4997baf5c5', (SELECT id FROM account WHERE email = 'luc.martin@conference.com'), (SELECT id FROM session WHERE date = '2023-06-01 10:00:00'), null),
    ('986c98d9-b28f-4272-87da-225c8f8b76d7', (SELECT id FROM account WHERE email = 'emilie.lefebvre@conference.com'), (SELECT id FROM session WHERE date = '2023-06-01 10:00:00'), now()),
    ('32dcba3d-8c0e-44a2-9e56-16f85ab8d5db', (SELECT id FROM account WHERE email = 'pierre.rousseau@conference.com'), (SELECT id FROM session WHERE date = '2023-06-02 14:00:00'), now()),

    -- Réservations pour MktgConf 2023
    ('1a4da4c4-8edf-4a21-9a3f-7bb3d01ab3b7', (SELECT id FROM account WHERE email = 'marie.dupont@conference.com'), (SELECT id FROM session WHERE date = '2023-07-10 09:00:00'), null),
    ('5b5a5b8a-7566-4c3e-8357-3e8d79e7e818', (SELECT id FROM account WHERE email = 'luc.martin@conference.com'), (SELECT id FROM session WHERE date = '2023-07-10 09:00:00'), null),
    ('5a5b5a8a-e7e7-4e3e-bc7f-8a8d5b5a5a5b', (SELECT id FROM account WHERE email = 'emilie.lefebvre@conference.com'), (SELECT id FROM session WHERE date = '2023-07-11 13:00:00'), null),

    -- Réservations pour Summit 2023
    ('15b2f9a9-c02c-4b14-a0c7-fdb1d702a28a', (SELECT id FROM account WHERE email = 'luc.martin@conference.com'), (SELECT id FROM session WHERE date = '2023-08-20 11:00:00'), now()),
    ('72c860aa-9d5f-4f09-b6dd-8c5d5d52670f', (SELECT id FROM account WHERE email = 'emilie.lefebvre@conference.com'), (SELECT id FROM session WHERE date = '2023-08-20 11:00:00'), now()),
    ('6b3a1ce3-6ebd-43e7-85d1-6bfc10f24075', (SELECT id FROM account WHERE email = 'sophie.girard@conference.com'), (SELECT id FROM session WHERE date = '2023-08-21 15:00:00'), now()),

    -- Réservations pour Startup Expo 2023
    ('3de3a029-5a28-4f5c-bf5d-7d1b1165a5e5', (SELECT id FROM account WHERE email = 'marie.dupont@conference.com'), (SELECT id FROM session WHERE date = '2023-09-30 10:00:00'), now()),
    ('57e2d4b4-939e-4a1a-8d63-0aa6f9281e17', (SELECT id FROM account WHERE email = 'luc.martin@conference.com'), (SELECT id FROM session WHERE date = '2023-09-30 10:00:00'), null),
    ('8b0af054-6fc1-46e3-b6e8-6de25bf48c2f', (SELECT id FROM account WHERE email = 'pierre.rousseau@conference.com'), (SELECT id FROM session WHERE date = '2023-10-01 14:00:00'), now()),

    -- Réservations pour BizConf 2023
    ('c68d8b77-3201-4b05-9c12-41d74a71293c', (SELECT id FROM account WHERE email = 'emilie.lefebvre@conference.com'), (SELECT id FROM session WHERE date = '2023-11-10 09:00:00'), null),
    ('7c38d081-8f7d-4c16-b0e7-98895ed22d2a', (SELECT id FROM account WHERE email = 'pierre.rousseau@conference.com'), (SELECT id FROM session WHERE date = '2023-11-10 09:00:00'), null),
    ('67cbf7d8-6a8e-4e9c-9c6c-f3a129ed37d2', (SELECT id FROM account WHERE email = 'sophie.girard@conference.com'), (SELECT id FROM session WHERE date = '2023-11-11 13:00:00'), now());

