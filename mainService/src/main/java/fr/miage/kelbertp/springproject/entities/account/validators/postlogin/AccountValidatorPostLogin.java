package fr.miage.kelbertp.springproject.entities.account.validators.postlogin;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class AccountValidatorPostLogin {
    private final Validator validator;

    AccountValidatorPostLogin(Validator validator) {
        this.validator = validator;
    }

    public void validate(AccountLoginInput accountLogin) {
        Set<ConstraintViolation<AccountLoginInput>> violations = validator.validate(accountLogin);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }
}
