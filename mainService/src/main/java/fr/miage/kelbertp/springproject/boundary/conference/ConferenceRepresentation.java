package fr.miage.kelbertp.springproject.boundary.conference;

import fr.miage.kelbertp.springproject.control.JwtCheck;
import fr.miage.kelbertp.springproject.control.assembler.ConferenceAssembler;
import fr.miage.kelbertp.springproject.entities.conference.Conference;
import fr.miage.kelbertp.springproject.entities.conference.validators.ConferenceInput;
import fr.miage.kelbertp.springproject.entities.conference.validators.ConferenceValidator;
import fr.miage.kelbertp.springproject.enums.Roles;
import jakarta.transaction.Transactional;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Valid;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@ResponseBody
@RequestMapping(value = "/conferences", produces = MediaType.APPLICATION_JSON_VALUE)
public class ConferenceRepresentation {

    private final ConferenceAssembler ca;
    private final ConferenceResource cr;
    private final ConferenceValidator cv;

    public ConferenceRepresentation(ConferenceAssembler ca, ConferenceResource cr, ConferenceValidator cv) {
        this.ca = ca;
        this.cr = cr;
        this.cv = cv;
    }

    @PostMapping
    @Transactional
    public ResponseEntity<Object> createConference(@RequestHeader("Authorization") String auth, @RequestBody @Valid ConferenceInput conference) {
        if (!JwtCheck.userCanAccess(auth, List.of(Roles.ROLE_ADMIN))) return ResponseEntity.status(401).build();

        Conference conferenceToSave = ca.toEntity(conference);

        Conference u = cr.save(conferenceToSave);

        URI uri = linkTo(ConferenceRepresentation.class).slash(u.getId()).toUri();

        return ResponseEntity.created(uri).body(ca.toModel(u));
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntityModel<Conference>> getConference(@PathVariable("id") String id) {
        return cr.findById(id)
                .map(ca::toModel)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/search")
    public ResponseEntity<CollectionModel<EntityModel<Conference>>> searchConferences(@RequestParam(value = "data") String data) {
        if (data.isBlank()) return ResponseEntity.badRequest().build();

        return ResponseEntity.ok(ca.toCollectionModel(cr.searchConference(data.toUpperCase())));
    }

    @GetMapping
    public ResponseEntity<CollectionModel<EntityModel<Conference>>> getConferences() {
        return ResponseEntity.ok(ca.toCollectionModel(cr.findAll()));
    }

    @PatchMapping("/{id}")
    @Transactional
    public ResponseEntity<Object> updateConference(@RequestHeader("Authorization") String auth, @PathVariable("id") String id, @RequestBody ConferenceInput conference) {
        if (!JwtCheck.userCanAccess(auth, List.of(Roles.ROLE_ADMIN))) return ResponseEntity.status(401).build();

        try {
            cv.validateProperties(conference);
        } catch (ConstraintViolationException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        Optional<Conference> conferenceToUpdate = cr.findById(id);
        if (conferenceToUpdate.isEmpty()) return ResponseEntity.notFound().build();

        conferenceToUpdate.map(u -> {
            if (conference.getName() != null) u.setName(conference.getName());
            if (conference.getDescription() != null) u.setDescription(conference.getDescription());
            if (conference.getSpeaker() != null) u.setSpeaker(conference.getSpeaker());
            if (conference.getLocation() != null) u.setLocation(conference.getLocation());
            return u;
        });

        return ResponseEntity.ok(ca.toModel(conferenceToUpdate.get()));
    }
}
