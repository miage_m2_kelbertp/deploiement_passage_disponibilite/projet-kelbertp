package fr.miage.kelbertp.springproject.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@AllArgsConstructor
@Data
public class AccountLogin {
    private String email;
    private String password;

    public static String getToken(String email, String password, MockMvc mock) throws Exception {
        MvcResult result = mock.perform(servletContext -> {
            MockHttpServletRequest request = new MockHttpServletRequest();
            request.setMethod("POST");
            request.setRequestURI("/accounts/login");
            request.setContentType(MediaType.APPLICATION_JSON_VALUE);
            request.setContent("{\"email\":\"".concat(email).concat("\",\"password\":\"").concat(password).concat("\"}").getBytes());
            return request;
        }).andReturn();

        // get token in json
        return "Bearer " + result.getResponse().getContentAsString().split("\"token\":\"")[1].split("\"")[0];
    }
}
