package fr.miage.kelbertp.springproject.entities.session.validator;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class SessionValidator {
    private final Validator validator;

    SessionValidator(Validator validator) {
        this.validator = validator;
    }

    public void validate(SessionInput session) {
        Set<ConstraintViolation<SessionInput>> violations = validator.validate(session);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }

    public void validateProperties(SessionInput session) {
        if (session.getCapacity() != 0) {
            this.validateProperty(session, "capacity");
        }

        if (session.getDate() != null) {
            this.validateProperty(session, "date");
        }

        if (session.getPrice() != 0) {
            this.validateProperty(session, "price");
        }
    }


    private void validateProperty(SessionInput session, String property) {
        Set<ConstraintViolation<SessionInput>> violations = validator.validateProperty(session, property);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }
}
