package fr.miage.kelbertp.springproject.control;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class JwtCheck {
    private JwtCheck() {
    }

    private static boolean isTokenValid(HashMap payloadMap) {
        return payloadMap.get("exp") != null && (long) payloadMap.get("exp") > new Date().getTime();
    }

    private static HashMap decodePayload(String token) throws JsonProcessingException {
        String[] tokenParts = token.split("\\.");
        String payload = tokenParts[1];
        String payloadJson = new String(Base64.getDecoder().decode(payload));
        return new ObjectMapper().readValue(payloadJson, HashMap.class);
    }

    public static boolean userCanAccess(String auth, List<String> roles) {
        try {
            HashMap payloadMap = decodePayload(auth);
            if (!isTokenValid(payloadMap)) {
                return false;
            }
            String role = (String) payloadMap.get("role");
            return roles.contains(role);
        } catch (Exception e) {
            return false;
        }
    }

    public static String getId(String auth) {
        try {
            HashMap payloadMap = decodePayload(auth);
            return (String) payloadMap.get("id");
        } catch (JsonProcessingException e) {
            return null;
        }
    }
}
