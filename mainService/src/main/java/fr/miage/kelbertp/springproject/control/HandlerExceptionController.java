package fr.miage.kelbertp.springproject.control;

import fr.miage.kelbertp.springproject.entities.ApiError;
import jakarta.validation.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.net.ConnectException;
import java.util.List;

@RestControllerAdvice
public class HandlerExceptionController {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public List<ApiError> handleValidationExceptions(MethodArgumentNotValidException ex) {
        return ApiError.fromBindingErrors(ex.getBindingResult());
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(DataIntegrityViolationException.class)
    public List<ApiError> handleValidationExceptions(DataIntegrityViolationException ex) {
        return ApiError.fromDataIntegrityViolationCause(ex.getCause());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentException.class)
    public List<ApiError> handleValidationExceptions(ConstraintViolationException ex) {
        return ApiError.fromConstraintViolationException(ex);
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(MissingRequestHeaderException.class)
    public List<ApiError> handleValidationExceptions(MissingRequestHeaderException ex) {
        return ApiError.fromMissingRequestHeaderException(ex);
    }

    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler(ConnectException.class)
    public List<ApiError> handleValidationExceptions(ConnectException ex) {
        return ApiError.fromConnectException(ex);
    }
}
