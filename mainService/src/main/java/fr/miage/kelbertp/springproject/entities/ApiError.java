package fr.miage.kelbertp.springproject.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class ApiError {
    private String message;

    public static List<ApiError> fromBindingErrors(BindingResult bindingResult) {
        return bindingResult.getFieldErrors().stream()
                .map(error -> new ApiError(error.getField() + " : " + error.getDefaultMessage()))
                .collect(Collectors.toList());
    }

    public static List<ApiError> fromDataIntegrityViolationCause(Throwable ex) {
        return List.of(new ApiError(ex.getCause().getLocalizedMessage()));
    }

    public static List<ApiError> fromConstraintViolationException(Throwable ex) {
        return List.of(new ApiError(ex.getCause().getLocalizedMessage()));
    }

    public static List<ApiError> fromMissingRequestHeaderException(Throwable ex) {
        return List.of(new ApiError(ex.getMessage()));
    }

    public static List<ApiError> fromConnectException(Throwable ex) {
        return List.of(new ApiError(ex.getMessage()));
    }
}