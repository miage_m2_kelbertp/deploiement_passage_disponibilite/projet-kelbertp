package fr.miage.kelbertp.springproject.control;

import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

import static fr.miage.kelbertp.springproject.enums.ErrorMessages.BANK_UNAVAILABLE;

public class ExternalCall {
    private ExternalCall() {
    }

    public static ResponseEntity<String> call(RestTemplate restTemplate, String url, HttpMethod method, String json) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(json, headers);

        try {
            return restTemplate.exchange(url, method, entity, String.class);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().is4xxClientError()) {
                return ResponseEntity.status(e.getStatusCode()).body(e.getResponseBodyAsString());
            }
        } catch (Exception e) {
            Logger.getLogger(ExternalCall.class.getName()).warning(e.getMessage());
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(BANK_UNAVAILABLE);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}
