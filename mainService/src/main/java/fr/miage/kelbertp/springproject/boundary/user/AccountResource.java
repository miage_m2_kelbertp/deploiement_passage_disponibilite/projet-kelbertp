package fr.miage.kelbertp.springproject.boundary.user;

import fr.miage.kelbertp.springproject.entities.account.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AccountResource extends JpaRepository<Account, String> {
    @Query("SELECT a FROM Account a WHERE a.email = :email AND a.password = :password")
    Account findAccountByEmailAndPassword(@Param("email") String email0, @Param("password") String password);
}
