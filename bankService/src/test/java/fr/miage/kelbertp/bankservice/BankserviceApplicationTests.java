package fr.miage.kelbertp.bankservice;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockHttpServletRequestDsl;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {BankserviceApplication.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class BankserviceApplicationTests {

	@Autowired
	private WebApplicationContext webApplicationContext;
	private MockMvc mockMvc;

	@BeforeAll
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	void testPostBankAccountUpdate() throws Exception {
		mockMvc.perform(servletContext -> {
			MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
			request.setMethod("POST");
			request.setRequestURI("/banks/1");
			request.setContent("{\"amount\": 100}".getBytes());
			request.setContentType("application/json");
			return request;
		}).andExpect(status().isOk())
		.andExpect(jsonPath("$.pay").value(407.5));
	}

	@Test
	void testPostBankAccountUpdateWithNotEnoughMoney() throws Exception {
		mockMvc.perform(servletContext -> {
			MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
			request.setMethod("POST");
			request.setRequestURI("/banks/1");
			request.setContent("{\"amount\": -500}".getBytes());
			request.setContentType("application/json");
			return request;
		}).andExpect(status().isConflict())
		.andExpect(jsonPath("$.message").value("No enough money"));
	}

	@Test
	void testPostBankAccountUpdateWithBankAccountNotFound() throws Exception {
		mockMvc.perform(servletContext -> {
			MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
			request.setMethod("POST");
			request.setRequestURI("/banks/aaa");
			request.setContent("{\"amount\": 100}".getBytes());
			request.setContentType("application/json");
			return request;
		}).andExpect(status().isNotFound())
		.andExpect(jsonPath("$.message").value("Bank account not found"));
	}

	@Test
	void testGetBankAccount() throws Exception {
		mockMvc.perform(servletContext -> {
			MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
			request.setMethod("GET");
			request.setRequestURI("/banks/2");
			return request;
		}).andExpect(status().isOk())
		.andExpect(jsonPath("$.pay").value(213.8));
	}

	@Test
	void testGetBankAccountWithBankAccountNotFound() throws Exception {
		mockMvc.perform(servletContext -> {
			MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
			request.setMethod("GET");
			request.setRequestURI("/banks/aaa");
			return request;
		}).andExpect(status().isNotFound())
		.andExpect(jsonPath("$.message").value("Bank account not found"));
	}

	@Test
	void testPostBankAccount() throws Exception {
		mockMvc.perform(servletContext -> {
			MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
			request.setMethod("POST");
			request.setRequestURI("/banks/10/create");
			request.setContentType("application/json");
			return request;
		}).andExpect(status().isOk())
		.andExpect(jsonPath("$.pay").value(0));
	}
}
