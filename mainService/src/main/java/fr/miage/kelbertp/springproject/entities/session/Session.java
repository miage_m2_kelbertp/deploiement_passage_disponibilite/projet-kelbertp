package fr.miage.kelbertp.springproject.entities.session;

import fr.miage.kelbertp.springproject.entities.conference.Conference;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
@Table(name = "session")
public class Session {
    @Id
    private String id;
    @ManyToOne
    @JoinColumn(name = "conference_id")
    private Conference conference;
    private int capacity;
    private double price;
    private String date;

    public Session(int capacity, double price, String date) {
        this.capacity = capacity;
        this.price = price;
        this.date = date;
    }

    public Session(Conference conference, int capacity, double price, String date) {
        this.conference = conference;
        this.capacity = capacity;
        this.price = price;
        this.date = date;
    }
}
