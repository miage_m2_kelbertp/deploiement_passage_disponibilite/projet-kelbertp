package fr.miage.kelbertp.springproject.entities.account.validators.accountinput;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class AccountValidator {
    private final Validator validator;

    AccountValidator(Validator validator) {
        this.validator = validator;
    }

    public void validate(AccountInput account) {
        Set<ConstraintViolation<AccountInput>> violations = validator.validate(account);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }

    public void validateProperties(AccountInput account) {
        if (account.getLastName() != null) {
            this.validateProperty(account, "lastName");
        }

        if (account.getFirstName() != null) {
            this.validateProperty(account, "firstName");
        }

        if (account.getEmail() != null) {
            this.validateProperty(account, "email");
        }

        if (account.getPassword() != null) {
            this.validateProperty(account, "password");
        }
    }

    private void validateProperty(AccountInput account, String property) {
        Set<ConstraintViolation<AccountInput>> violations = validator.validateProperty(account, property);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }
}
